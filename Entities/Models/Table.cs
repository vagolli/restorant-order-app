﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class Table
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Table Area")]
        public int Area { get; set; }

        [Display(Name = "Table Status")]
        public bool IsBusy { get; set; }
    }

    public struct Area
    {
        public const int Indoor = 0;
        public const int Outdoor = 1;
        public const int Garden = 2;
    }
}
