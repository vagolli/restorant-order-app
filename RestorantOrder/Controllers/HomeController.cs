﻿using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using Entities.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
//using RestorantOrder.Data;
using RestorantOrder.Models;

namespace RestorantOrder.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: RestaurantArea
        public async Task<IActionResult> Index()
        {
            return View(await _context.Tables.ToListAsync());
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
