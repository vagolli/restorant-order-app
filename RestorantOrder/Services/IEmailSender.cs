﻿using System.Threading.Tasks;

namespace RestorantOrder.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
