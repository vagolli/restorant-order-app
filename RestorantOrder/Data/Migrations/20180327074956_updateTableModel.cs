﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace RestorantOrder.Data.Migrations
{
    public partial class updateTableModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Indoors",
                table: "Tables");

            migrationBuilder.AddColumn<int>(
                name: "Area",
                table: "Tables",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Area",
                table: "Tables");

            migrationBuilder.AddColumn<bool>(
                name: "Indoors",
                table: "Tables",
                nullable: false,
                defaultValue: false);
        }
    }
}
