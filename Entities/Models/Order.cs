﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class Order
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Order Items:")]
        public List<Item> Items { get; set; }

        [Required]
        [Display(Name = "Waiter:")]
        public string WaiterId { get; set; }

        [Required]
        [Display(Name = "Table")]
        public Table Table { get; set; }

        [Required]
        [Display(Name = "Order Price")]
        public decimal TotalOrderPrice { get; set; }

        private string date = DateTime.Now.ToString("MM/dd/yy H:mm:ss");

        [Required]
        [Display(Name = "Order Date")]
        public string Date
        {
            private set { date = DateTime.Now.ToString("MM/dd/yy H:mm:ss"); }
            get { return date; }
        }
    }
}
